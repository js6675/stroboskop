# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://js6675@bitbucket.org/js6675/stroboskop.git
git init
```

Naloga 6.2.3:
https://bitbucket.org/js6675/stroboskop/commits/7cd32d8551506269bc68385cb8ca2e276c4788a1?at=master

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/js6675/stroboskop/commits/066ef68017795179d24512bcf8a7d811c38a2364?at=master

Naloga 6.3.2:
https://bitbucket.org/js6675/stroboskop/commits/117d6f0cce88b37043eae0c1fa5360c9d532f36a?at=master

Naloga 6.3.3:
https://bitbucket.org/js6675/stroboskop/commits/de10d4aa826df4af0789cbe19919c95e4e89b5e6?at=master

Naloga 6.3.4:
https://bitbucket.org/js6675/stroboskop/commits/78d44b75b3fc6bea2a5420fb76b42bb3d254c016?at=master

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/js6675/stroboskop/commits/8e2e50d9f346cb5567b44c77cbe67414f32ab7ce?at=master

Naloga 6.4.2:
https://bitbucket.org/js6675/stroboskop/commits/9a11aea275c8fca74f88f968e2598c1b4f30caa2?at=master

Naloga 6.4.3:
https://bitbucket.org/js6675/stroboskop/commits/d8bf9b0396a687a3a3bcc32fd0bb11b7e52d0bf5?at=master

Naloga 6.4.4:
https://bitbucket.org/js6675/stroboskop/commits/8bc06bfe61dd28677a94247f1fe62cbd834c4ce2?at=master